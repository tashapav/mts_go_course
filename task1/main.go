package main

import (
	"fmt"
)

type Book struct {
	Id     int
	Title  string
	Author string
	Year   int
}

type Storage interface {
	AddBook(Book) error
	GetBookByID(int) (Book, error)
	GetId(string) int
}

type Library struct {
	storage Storage
}

func (b *Book) GetTitle() string {
	return b.Title
}

func (b *Book) GetAuthor() string {
	return b.Author
}

func (b *Book) GetYear() int {
	return b.Year
}

type MapStorage struct {
	booksMap    map[int]Book
	idGenerator func(string) int
}

func (s *MapStorage) AddBook(book Book) error {
	id := s.idGenerator(book.Title)
	book.Id = id
	_, ok := s.booksMap[id]
	if ok == true {
		return fmt.Errorf("При добавлении произошла ошибка")
	} 
	s.booksMap[id] = book
	return nil
}

func (s *MapStorage) GetId(title string) int {
	return s.idGenerator(title)
}

func (s *MapStorage) GetBookByID(id int) (Book, error) {
	book, ok := s.booksMap[id]
	if ok == false {
		return Book{}, fmt.Errorf("Такая книга не найдена")
	}
	return book, nil
}

type SliceStorage struct {
	booksSlice  []Book
	idGenerator func(string) int
}

func (s *SliceStorage) AddBook(book Book) error {
	id := s.idGenerator(book.Title)
	book.Id = id
	_, err := s.GetBookByID(id)
	if err != nil {
		s.booksSlice = append(s.booksSlice, book)
	}
	return fmt.Errorf("При добавлении произошла ошибка")
}

func (s *SliceStorage) GetId(title string) int {
	return s.idGenerator(title)
}

func (s *SliceStorage) GetBookByID(id int) (Book, error) {
	for _, book := range s.booksSlice {
		if book.Id == id {
			return book, nil
		}
	}
	return Book{}, fmt.Errorf("Такая книга не найдена ")
}

func (l *Library) AddBook(book Book) {
	l.storage.AddBook(book)
}

func (l *Library) GetBookByTitle(title string) (Book, error) {
	id := l.storage.GetId(title)
	return l.storage.GetBookByID(id)
}

func NewMapStorage() MapStorage {
	return MapStorage{
		booksMap:    map[int]Book{},
		idGenerator: idGeneratorMap,
	}
}

func NewSliceStorage() SliceStorage {
	return SliceStorage{
		booksSlice:  []Book{},
		idGenerator: idGeneratorSlice,
	}
}

func NewLibrary(storage Storage) Library {
	return Library{
		storage,
	}
}

func idGeneratorMap(title string) int {
	id := 0
	for _, letter := range title {
		id = (id*7 + int(letter)) % 84567321
	}
	return id
}

func idGeneratorSlice(title string) int {
	id := 0
	for _, letter := range title {
		id = (id*7 + int(letter)) % 84567321
	}
	return id
}

func main() {

	allBooks := [5]Book{
		{Title: "Недоросль", Author: "Денис Фонвизин", Year: 1782},
		{Title: "Рассказы", Author: "Генри", Year: 1904},
		{Title: "Обыкновенное чудо", Author: "Евгений Шварц", Year: 1956},
		{Title: "Затерянный мир", Author: "Артур Конан Дойль", Year: 1912},
		{Title: "Улитка на склоне", Author: "Аркадий и Борис Стругацкие ", Year: 1966},
	}

	mapStorage := NewMapStorage()
	sliceStorage := NewSliceStorage()
	libraryMap := NewLibrary(&mapStorage)
	librarySlice := NewLibrary(&sliceStorage)

	for i := 0; i < len(allBooks); i++ {
		libraryMap.AddBook(allBooks[i])
	}

	fmt.Println(libraryMap.GetBookByTitle("Недоросль"))
	fmt.Println(libraryMap.GetBookByTitle("Затерянный мир"))

	for i := 0; i < len(allBooks); i++ {
		librarySlice.AddBook(allBooks[i])
	}

	fmt.Println(librarySlice.GetBookByTitle("Недоросль"))
	fmt.Println(librarySlice.GetBookByTitle("Затерянный мир"))
}
